# Urdu translation for linuxmint
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2016-06-15 11:46+0100\n"
"PO-Revision-Date: 2016-09-23 10:30+0000\n"
"Last-Translator: Waqar Ahmed <waqar.17a@gmail.com>\n"
"Language-Team: Urdu <ur@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2016-12-12 12:15+0000\n"
"X-Generator: Launchpad (build 18292)\n"

#: usr/lib/blueberry/blueberry.py:68 usr/lib/blueberry/blueberry-tray.py:35
#: usr/lib/blueberry/blueberry-tray.py:52
#: usr/lib/blueberry/blueberry-tray.py:67 generate_desktop_files:25
msgid "Bluetooth"
msgstr "بلیو ٹوتھ"

#: usr/lib/blueberry/blueberry.py:82
msgid "Bluetooth is disabled"
msgstr "بلوٹوتھ غیر فعال ہے"

#: usr/lib/blueberry/blueberry.py:83
msgid "No Bluetooth adapters found"
msgstr "بلوٹوتھ ایڈاپٹر نہیں ملے"

#: usr/lib/blueberry/blueberry.py:84
msgid "Bluetooth is disabled by hardware switch"
msgstr "بلوٹوتھ ہارڈ وئیر بٹن سے بند ہے"

#: usr/lib/blueberry/blueberry.py:119
msgid "Show a tray icon"
msgstr "ٹرے آئکن دکھائیں"

#: usr/lib/blueberry/blueberry-tray.py:62
#, python-format
msgid "Bluetooth: %d device connected"
msgid_plural "Bluetooth: %d devices connected"
msgstr[0] "بلیو ٹوتھ: %d ڈیوائس منسلک"
msgstr[1] "بلیو ٹوتھ: %d ڈیوائسیں منسلک"

#: usr/lib/blueberry/blueberry-tray.py:109
msgid "Send files to a device"
msgstr "فائلیں ڈیوائس میں منتقل کریں"

#: usr/lib/blueberry/blueberry-tray.py:113
msgid "Open Bluetooth device manager"
msgstr "بلیو ٹوتھ ڈیوائس منتظم کھولیں"

#: usr/lib/blueberry/blueberry-tray.py:119
msgid "Quit"
msgstr "بند کریں"

#: generate_desktop_files:25
msgid "Configure Bluetooth settings"
msgstr "بلوٹوتھ ترتیبات تشکیل دیں"
